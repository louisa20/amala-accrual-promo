# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/accrual-promo.jar /accrual-promo.jar
# run application with this command line[
CMD ["java", "-jar", "/accrual-promo.jar"]
