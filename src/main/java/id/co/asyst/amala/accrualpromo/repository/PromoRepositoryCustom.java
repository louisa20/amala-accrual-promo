package id.co.asyst.amala.accrualpromo.repository;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.commons.core.repository.RepositoryCustom;

public interface PromoRepositoryCustom extends RepositoryCustom<AccrualPromo> {
}
