package id.co.asyst.amala.accrualpromo.service;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.amala.accrualpromo.repository.PromoRepository;
import id.co.asyst.commons.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PromoServiceImpl extends BaseService<AccrualPromo, String> implements PromoService {
    @Autowired
    private final PromoRepository promoRepository;

    @Autowired
    private PromoService promoService;

    public PromoServiceImpl(PromoRepository promoRepository) {
        this.promoRepository = promoRepository;
        this.setRepository(promoRepository);
        this.setCustomRepository(promoRepository);
    }
}
