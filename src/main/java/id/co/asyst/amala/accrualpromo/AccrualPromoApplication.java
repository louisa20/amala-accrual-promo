package id.co.asyst.amala.accrualpromo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"id.co.asyst.commons", "id.co.asyst.amala"})
@EntityScan({"id.co.asyst.amala"})
@EnableDiscoveryClient
public class AccrualPromoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccrualPromoApplication.class, args);
    }

}
