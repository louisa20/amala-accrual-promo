package id.co.asyst.amala.accrualpromo.repository;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PromoRepository extends BaseRepository<AccrualPromo, String>, PromoRepositoryCustom {
}
