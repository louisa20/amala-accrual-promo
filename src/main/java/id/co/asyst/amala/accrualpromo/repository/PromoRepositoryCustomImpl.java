package id.co.asyst.amala.accrualpromo.repository;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.commons.core.repository.BaseRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class PromoRepositoryCustomImpl extends BaseRepositoryCustom<AccrualPromo> implements PromoRepositoryCustom {
}
