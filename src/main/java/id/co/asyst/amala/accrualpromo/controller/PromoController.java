package id.co.asyst.amala.accrualpromo.controller;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.commons.core.controller.BaseController;
import id.co.asyst.commons.core.payload.BaseParameter;
import id.co.asyst.commons.core.payload.BaseRequest;
import id.co.asyst.commons.core.payload.BaseResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Aug 07, 2020
 * @since 1.2.0-SNAPSHOT
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/promo")
public class PromoController extends BaseController {
    private static final Logger logger = LogManager.getLogger();

    @PostMapping("${controller.mapping.operation.create}")
    public BaseResponse<AccrualPromo> create(@Valid @RequestBody BaseRequest<BaseParameter<AccrualPromo>> request) {
        BaseResponse<AccrualPromo> accrualPromoResponse = new BaseResponse<AccrualPromo>();

        return accrualPromoResponse;
    }
}
