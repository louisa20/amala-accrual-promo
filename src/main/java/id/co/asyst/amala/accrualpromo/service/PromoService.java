package id.co.asyst.amala.accrualpromo.service;

import id.co.asyst.amala.accrual.promo.model.AccrualPromo;
import id.co.asyst.commons.core.service.Service;

public interface PromoService extends Service<AccrualPromo, String> {
}
